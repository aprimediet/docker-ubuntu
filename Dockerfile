ARG UBUNTU_VERSION=noble

FROM ubuntu:${UBUNTU_VERSION} AS base
LABEL maintainer="<Aditya Prima> aprimediet@gmail.com"

ARG UBUNTU_VERSION=noble

ENV DEBIAN_FRONTEND noninteractive

# ADJUST LOCAL TIME
ENV TZ Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Set User as root
USER root

# Install base dependencies
RUN --mount=type=cache,target=/var/cache/apt/archives \
    apt -y update && apt -y upgrade && apt -y install curl

# Clean APT Cache
RUN apt -y clean
